const should = require('should');
const supertest = require('supertest');

let request = supertest('http://localhost:3000');
// Require the app to start a node instance to make the http requests to the app.
let app = require('../app');
// Data to mimic the form submition data.
let postTask = {
  name: 'Testing Task',
  description: 'Hello Task',
  dueDate: '10/20/2017',
};

describe('Testing Tasks routes', () => {

  it('responds to /tasks with 200 status code', (done) => {
    request.get('/tasks').expect(200).end( (err,res) => {
      should.not.exist(err);
      should.exist(res.body.data);
      done();
    });
  });

  it('responds to GET /tasks/add with a 404 status code', (done) => {
    request.get('/task/add').expect(404, done);
  });

  it('responds to /tasks/add with 200 status code and add a task to the list', (done) => {
    request.post('/tasks/add').send(postTask).expect(200).end( (err,res) => {
      should.not.exist(err);
      should.exist(res.body.message);
      res.body.message.should.equal('Task was added successfully.');
      should.exist(res.body.data);
      let testTask = res.body.data;
      testTask.should.have.property('id');
      testTask.should.have.property('name');
      testTask.name.should.equal(postTask.name);
      testTask.should.have.property('description');
      testTask.description.should.equal(postTask.description);
      testTask.should.have.property('dueDate');
      testTask.dueDate.should.equal(postTask.dueDate);
      testTask.should.have.property('completedDate');
      testTask.completedDate.should.be.empty();
      postTask = Object.assign({}, postTask, res.body.data);
      done();
    });
  });

  it('responds to /tasks with 200 status code and our test object', (done) => {
    request.get('/tasks').expect(200).end( (err,res) => {
      should.not.exist(err);
      should.exist(res.body.data);
      should.exist(res.body.data[postTask.id]);
      let testTask = res.body.data[postTask.id];
      testTask.should.have.property('id');
      testTask.id.should.equal(postTask.id);
      testTask.should.have.property('name');
      testTask.name.should.equal(postTask.name);
      testTask.should.have.property('description');
      testTask.description.should.equal(postTask.description);
      testTask.should.have.property('dueDate');
      testTask.dueDate.should.equal(postTask.dueDate);
      testTask.should.have.property('completedDate');
      testTask.completedDate.should.be.empty();
      done();
    });
  });

  it('responds to /tasks/invalid with 404 when using an invalid id', (done) => {
    request.get('/tasks/invalid').expect(404, done);
  });

  it('responds to /tasks/update/:id with 404 when using an invalid id', (done) => {
    request.post('/tasks/update/invalid').send({id:'invalid'}).expect(404, done);
  });

  it('responds to /tasks/:id with 200 status code and our test object', (done) => {
    request.get('/tasks/'+postTask.id).expect(200).end( (err,res) => {
      should.not.exist(err);
      should.exist(res.body.data);
      let testTask = res.body.data;
      testTask.should.have.property('id');
      testTask.id.should.equal(postTask.id);
      testTask.should.have.property('name');
      testTask.name.should.equal(postTask.name);
      testTask.should.have.property('description');
      testTask.description.should.equal(postTask.description);
      testTask.should.have.property('dueDate');
      testTask.dueDate.should.equal(postTask.dueDate);
      testTask.should.have.property('completedDate');
      testTask.completedDate.should.be.empty();
      done();
    });
  });

  it('responds to /tasks/update/:id with 200 status code and updates test object with a completed date', (done) => {
    postTask.completedDate = '10/19/2017';
    request.post('/tasks/update/'+postTask.id).send(postTask).expect(200).end( (err,res) => {
      should.not.exist(err);
      should.exist(res.body.message);
      res.body.message.should.equal('Task has been updated successfully.');
      should.exist(res.body.data);
      let testTask = res.body.data;
      testTask.should.have.property('id');
      testTask.id.should.equal(postTask.id);
      testTask.should.have.property('name');
      testTask.name.should.equal(postTask.name);
      testTask.should.have.property('description');
      testTask.description.should.equal(postTask.description);
      testTask.should.have.property('dueDate');
      testTask.dueDate.should.equal(postTask.dueDate);
      testTask.should.have.property('completedDate');
      testTask.completedDate.should.equal(postTask.completedDate);
      postTask = Object.assign({}, postTask, testTask);
      done();
    });
  });

  it('responds to /tasks/:id with 200 status code after updating test object', (done) => {
    request.get('/tasks/'+postTask.id).expect(200).end( (err,res) => {
      should.not.exist(err);
      should.exist(res.body.data);
      let testTask = res.body.data;
      testTask.should.have.property('id');
      testTask.id.should.equal(postTask.id);
      testTask.should.have.property('name');
      testTask.name.should.equal(postTask.name);
      testTask.should.have.property('description');
      testTask.description.should.equal(postTask.description);
      testTask.should.have.property('dueDate');
      testTask.dueDate.should.equal(postTask.dueDate);
      testTask.should.have.property('completedDate');
      testTask.completedDate.should.equal(postTask.completedDate);
      done();
    });
  });

  it('responds to /tasks/delete/:id with 200 status code and deletes the test object', (done) => {
    request.post('/tasks/delete').send({id: postTask.id}).expect(200).end( (err,res) => {
      should.not.exist(err);
      should.exist(res.body.message);
      res.body.message.should.equal('Task has been deleted successfully.');
      done();
    });
  });
});
