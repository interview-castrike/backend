const supertest = require('supertest');
let request = supertest('http://localhost:3000');
// Require the app to start a node instance to make the http requests to the app.
let app = require('../app');

describe('Testing root route', () => {
  it('responds to / with 200 status code', (done) => {
    request.get('/').expect(200, done);
  });

  it('responds to /not-a-task with 404 status code', (done) => {
    request.get('/not-a-task').expect(404, done);
  });
});
