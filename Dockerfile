FROM node:7

RUN apt-get update && apt-get -y install apt-transport-https \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update && apt-get install yarn

RUN mkdir -p /var/app
WORKDIR /var/app
COPY . /var/app

RUN yarn install

EXPOSE 3000

CMD ["node","app.js"]
