/**
 * IBM Sample App Router
 * Router handles CRUD functionality for tasks.
 * Tasks are stored using couchdb.
 */
const Router = require('koa-router');
const couchDb = require('node-couchdb');

const task = require('../models/Task');

const databaseName = process.env.DATABASE_NAME;
const db = new couchDb({
  host: process.env.DATABASE_HOST || '',
  port: process.env.DATABASE_PORT || '',
  auth: {
    user: process.env.DATABASE_USER || '',
    pass: process.env.DATABASE_PASSWORD || ''
  }
});
/*

*/

let router = new Router();

/**
 * / - Generic root route.
 */
router.get('/', (ctx) => {
  ctx.body = 'IBM Interview Sample App.';
});


/**
 * /tasks - access the tasks object stored in couchdb and return it as a response
 * Return a 500 error if there was an issue retrieving the tasks.
 */
router.get('/tasks', async (ctx) => {
  try {
    const tasksResult = await db.mango( databaseName, {
        "selector": {
          "_id": {
            "$gt": null
          }
        }
      } );
    let tasks = {};
    tasksResult.data.docs.forEach( (task) => {
      tasks[task['_id']] = task.field;
    });
    ctx.body = {
      data: tasks
    };
  } catch(err) {
    console.log( err );
    ctx.status = 500;
    ctx.body = 'Error occurred while retrieving the tasks.';
  }

});

/**
 * /tasks/:id - access the task object stored in couchDb with a given id.
 * Return this object as a response if it is part of the object.
 * Otherwise, return a 404 response if the task was not in the database or a 500 error if an id wasn't provided.
 */
router.get('/tasks/:id', async (ctx) => {
  if( ctx.params && ctx.params.id ) {
    try {
      const taskResult = await db.get( databaseName, ctx.params.id );
      ctx.body = {
        data: taskResult.data.field
      };
    } catch(err) {
      console.error( err );
      ctx.status = 404;
      ctx.body = 'Task was not found.';
    }
  } else {
    ctx.status = 500;
    ctx.body = 'A task id must be provided.';
  }
});

/**
 * /tasks/update/:id - update the task object stored in the couchdb a given id.
 * Return this object, after being updated.
 * Otherwise, return a 404 response if the task was not in the database or a 500 error if an id wasn't provided.
 */
router.post('/tasks/update/:id', async (ctx) => {
  if( ctx.params && ctx.params.id ) {
    try {
      const taskResult = await db.get( databaseName, ctx.params.id );
      const data = Object.assign( {}, taskResult.data.field, ctx.request.body );
      const updatedTask = await db.update( databaseName, {
          _id: taskResult.data._id,
          _rev: taskResult.data._rev,
          field: data
      } );
      if(updatedTask.data.ok) {
        ctx.body = {
          message: 'Task has been updated successfully.',
          data
        };
      } else {
        console.error(updatedTask);
        ctx.status = 500;
        ctx.body = 'Task has not been updated.';
      }


    } catch(err) {
      console.error( err );
      ctx.status = 404;
      ctx.body = 'Task was not found.';
    }
  } else {
    ctx.status = 500;
    ctx.body = 'A task id must be provided.';
  }
});

/**
 * /tasks/delete - delete the task object stored in couchdb with a given id which is sent as a POST parameter.
 * Otherwise, return a 404 response if the task was not in the database or a 500 error if an id wasn't provided.
 */
router.post('/tasks/delete', async (ctx) => {
  if( ctx.request.body && ctx.request.body.id ) {
    try {
      const taskResult = await db.get( databaseName, ctx.request.body.id );
      const deleteTask = await db.del( databaseName,  taskResult.data._id, taskResult.data._rev);
      if(deleteTask.data.ok) {
        ctx.body = {
          message: 'Task has been deleted successfully.'
        }
      } else {
        ctx.status = 500;
        ctx.body = 'Task has been not deleted.';
        console.error( deleteTask );
      }
    } catch(err) {
      console.error( err );
      ctx.status = 404;
      ctx.body = 'Task was not found.';
    }
  } else {
    ctx.status = 500;
    ctx.body = 'A task id must be provided.';
  }
});

/**
 * /tasks/add - add a task to couchdb using couchdb's unique id.
 * Return the newly created object as the response.
 * Otherwise, return a 500 error.
 */
router.post('/tasks/add', async (ctx) => {
  const id = await db.uniqid();
  const newTask = Object.assign( {}, task, ctx.request.body, {id: id[0] } );
  try {
    await db.insert(databaseName, { _id: newTask.id.toString(), field: newTask } );
    ctx.body = {
      message: 'Task was added successfully.',
      data: newTask
    };
  } catch(err) {
    ctx.status = 500;
    ctx.body = 'Error occurred adding the task.';
  }
});

module.exports = router;
