const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
// router is a instance of a Koa Router with its routes already set.
const router = require('./lib/router');

const app = new Koa();

// Add koa-bodyparser middleware to parse post requests.
app.use( bodyParser() );

// Handle errors
app.use(async (ctx, next) => {
  try {
    ctx.set('Access-Control-Allow-Origin','*');
    await next();
  } catch(err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
  }
});

// Add router middleware to handle routes.
app.use( router.routes() );

app.use( router.allowedMethods() );

app.listen(3000);
