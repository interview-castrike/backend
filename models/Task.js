/**
 * Simplistic Task model
 */
const task = {
  id: '',
  name: '',
  description: '',
  dueDate: '',
  completedDate: ''
};

module.exports = task;
