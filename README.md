# IBM Sample App API for Managing Tasks

## Getting Started
This API was developed for IBM's Interview Sample Application.

Task Object Format:
```
{
    "id": "",
    "name": "",
    "description": "",
    "dueDate": "",
    "completedDate": ""
}
```

### Prerequisites
This API runs on node v7.10.1 and uses yarn as its package manager.
Moreover, it uses couchdb for its data storage and couchdb information must be passed as environment variables.
These varibles are: DATABASE_NAME, DATABASE_HOST, DATABASE_PORT, DATABASE_USER, DATABASE_PASSWORD

### Installing
run ```yarn install```

### Testing
run ```npm test```

### Using Docker
Docker passes to the node instance `couchdb.ibmsampleapp.com`. this domain must point to the couchdb instance IP address.
When launching the docker containers for the first time, since the compose file uses a public image, we have to go the docker instance using the web interface (http://couchdb.ibmsampleapp.com:5984/_utils/) and set up the instance as a single node with a username and password.
Since the environment variables are hardcoded into the compose file for demo purposes, the user and password should be set to admin.

## Requests & Responses
### GET /
Generic route.
Response body:
```
IBM Interview Sample App
```

### GET /tasks
Retrieve tasks that have been added to the application.
If there is an issue connecting to the couchdb instance, the system will return a response with a 500 status code. The error will be logged in the node end.

Response body:
```
{
    "data": {
        "58c21f55118b9c008": {
            "id": "58c21f55118b9c008",
            "name": "One of my tasks",
            "description": "This is one of my tasks.",
            "dueDate": "2017-10-16",
            "completedDate": ""
        },
        "6dbd46c5500583f": {
            "id": "6dbd46c5500583f",
            "name": "Another one of my tasks",
            "description": "This is another task.",
            "dueDate": "2017-10-16",
            "completedDate": "2017-10-15"
        },
    }
}
```
### GET /tasks/[id]
Retrieve task that has been added to the application by its id.
If an id is not provided, the api will default to get all of the tasks.
If an id is not found in the data storage, it will return a 404 response.

Response body:
```
{
    "data": {
        "id": "6dbd46c5500583f",
        "name": "Another one of my tasks",
        "description": "This is another task.",
        "dueDate": "2017-10-16",
        "completedDate": "2017-10-15"
    }
}
```
### POST /tasks/add
Add a task to to app. At the moment, there are no required fields and any information posted is mixed with the default Task object. The request must hve its content type set to `Content-type: application/x-www-form-urlencoded`. The response returns the object that was just added or, if there is an error, a 500 error response.
Sample Request Body:
`completedDate=&description=I%20am%20adding%20a%20new%20task%21&dueDate=2017-10-17&name=Adding%20a%20task`

Response Body
```
{
    "message":"Task was added successfully",
    "data":{
        "id":"58c21f55118b9c0086dbd46c55007094",
        "name":"Adding a task",
        "description":"I am adding a new task!",
        "dueDate":"2017-10-17",
        "completedDate":""
    }
}
```

### POST /tasks/update/[id]


### POST /tasks/delete
Delete a task from the application. In order the delete a task, the request must hve its content type set to `Content-type: application/x-www-form-urlencoded` and the body must have an id as one of the parameters, like `id=6dbd46c5500583f`.
If the id is not provided or the task was not able to be deleted, the api will return a 500 status code response. If the id is provided but not found, a 404 status code response will be returned.

Response body:
```
{
    "message": "Task has been deleted successfully"
}
```
